from django.shortcuts import render, redirect
from .forms import TaskListForm
from .models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url="/accounts/login/")
def create_task(request):
    create_task = Task.objects.all()
    if request.method == "POST":
        form = TaskListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskListForm()

    context = {"form": form, "create_task": create_task}
    return render(request, "tasks/task_create.html", context)


@login_required(login_url="/accounts/login/")
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": show_my_tasks,
    }
    return render(request, "tasks/task_list.html", context)
