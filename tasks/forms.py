from django.forms import ModelForm
from .models import Task


class TaskListForm(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
        list_display = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
