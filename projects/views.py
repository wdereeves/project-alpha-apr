from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from .forms import CreateProjectForm

# Create your views here.


@login_required(login_url="/accounts/login/")
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/project_list.html", context)


@login_required(login_url="/accounts/login/")
def show_project(request, id):
    show_project = Task.objects.filter(id=id)
    project = get_object_or_404(Project, id=id, owner=request.user)
    context = {"show_project": show_project, "project": project}
    return render(request, "projects/project_detail.html", context)


@login_required(login_url="/accounts/login/")
def create_project(request):
    create_project = Project.objects.all()
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"form": form, "create_project": create_project}
    return render(request, "projects/project_create.html", context)
